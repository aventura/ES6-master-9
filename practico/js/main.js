const url = `https://randomuser.me/api/?results=9`
fetch(url)
    .then(respuesta => respuesta.json())
    .then(function(data){                        
        console.log(data);
        var html = data.results.map(function(usuario){
            return ''+
            '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">'+
            '    <div class="thumbnail">'+
            '       <img src="'+usuario.picture.large+'" alt="'+usuario.gender+'" class="img-responsive img-thumbnail">'+
            '        <div class="caption">'+
            '        <label for="nombre"><span class="glyphicon glyphicon-user"></span> nombre: '+usuario.name.first+'</label><br>'+
            '        <label for="email"><span class="glyphicon glyphicon-envelope"></span> email: '+usuario.email+'</label><br>'+
            '        <p><a href="#ventana2" class="btn btn-success" data-toggle="modal">ver mas</a></p>'+
            '        </div>'+
            '    </div>'+
            '</div>';
        });
        document.getElementById('resultado').innerHTML=html.join('');
    })
    .catch(function(error){
        //console.log(error);
        var html=''+
        '<div class="alert alert-danger">'+
        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+
        '<strong>Error!!!</strong><br/>'+error+
        '</div>';
        document.getElementById('mensajes').innerHTML=html;
    });             