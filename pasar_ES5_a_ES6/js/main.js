    (function(){
            var api="http://it-ebooks-api.info/v1/";
            function search(texto){
                var url=api+'search/'+texto;                
                fetch(url)
                    .then(function(response){
                        return response.json();
                    })
                    .then(function(data){                        
                        console.log(data);
                        var html = data.Books.map(function(libro){
                            return ''+
                            '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">'+
                            '    <div class="thumbnail">'+
                            '       <img src="'+libro.Image+'" alt="'+libro.Title+'" class="img-responsive img-thumbnail">'+
                            '        <div class="caption">'+
                            '           <h3>'+libro.Title+'</h3>'+
                            '           <h4>'+libro.SubTitle+'</h4>'+
                            '            <p>'+libro.Description+'</p>'+
                            '            <p><a href="#" class="btn btn-primary">ver</a></p>'+
                            '        </div>'+
                            '    </div>'+
                            '</div>';
                        });
                        document.getElementById('resultado').innerHTML=html.join('');
                    })
                    .catch(function(error){
                        //console.log(error);
                        var html=''+
                        '<div class="alert alert-danger">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+
                        '<strong>Error!!!</strong><br/>'+error+
                        '</div>';
                        document.getElementById('mensajes').innerHTML=html;
                    });                
            } //search

            document.getElementById('formSearch').addEventListener("submit",function(event){
                event.preventDefault();
                var val=document.getElementById('txt').value;                
                search(val);
            }); //document.getElementById('formSearch').addEventListener("submit",function(event){
        })();
        